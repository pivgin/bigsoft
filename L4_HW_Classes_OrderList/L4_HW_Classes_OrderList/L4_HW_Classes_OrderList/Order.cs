﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L4_HW_Classes_OrderList
{
    class Order
    {
        
        public List<OrderList> Items = new List<OrderList>();

        public double totalPrice
        {
            get
            {
                double result = 0.0;
                foreach (var item in Items)
                {
                    result += item.Price * item.Quantity;
                }
                return result;
            }
           
        }

        public void AddItem(OrderList item)
        {
            this.Items.Add(item);   
        }

        public void RemoveItem(OrderList item)
        {
            this.Items.Remove(item);
        }
    }
}
