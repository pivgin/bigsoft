﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L4_HW_Classes_OrderList
{
    class OrderList
    {
        private double _price;
        //private double _total;
        //private int _quantity;

        public OrderList(Product product, double price, int quantity)
        {
            this.Item = product;
            this.Price = price;
            this.Quantity = quantity;
        }
        public Product Item { get; set; }

        public double Price
        {
            get { return this._price; }
            set { this._price = value; }
        }
        
        public int Quantity { get; set; }

        public double Total
        {
            get { return this.Price*this.Quantity; }
        }
    }
}
