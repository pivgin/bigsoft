﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace L4_HW_Classes_OrderList
{
    class Program
    {
        // some changes
        static void Main(string[] args)
        {
            Order newOrder = new Order();
            Console.WriteLine("Howdy, Customer!");
            const string key = "N";
            do
            {
                /*
                
                Product tv = new Product("TV", "Sony");
                var firstPurchase = new OrderList(tv, 1500, 2);

                Console.WriteLine(firstPurchase.Item.Model);
                Console.WriteLine(firstPurchase.Item.Type);
                Console.WriteLine(firstPurchase.Price);
                Console.WriteLine(firstPurchase.Quantity);

                Product dvd = new Product("DVD", "Philips");
                var secondPurchase = new OrderList(dvd, 500, 1);

                Order newOrder = new Order();
                newOrder.AddItem(firstPurchase);
                if (newOrder.totalPrice.Equals(3000)) Console.WriteLine("That's good, dude!");

                newOrder.AddItem(secondPurchase);
                if (newOrder.totalPrice.Equals(3500)) Console.WriteLine("OK!");
                else Console.WriteLine("Smth went wrong, man!");
                 
                 */


                Console.WriteLine("\nPlease, type in type of staff you want to buy \nfirst and then name of the brand!");
                string type = Console.ReadLine();
                string brand = Console.ReadLine();
                Product newProduct = new Product(type, brand);

                Console.WriteLine("Here is your item <{0} - {1}>", newProduct.Type, newProduct.Model);
                Console.WriteLine("\nHow many {0}s do you want?", newProduct.Type);
                int quantity = int.Parse(Console.ReadLine());


                //  TODO: Make real price!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                Random rnd = new Random();
                double price = 0.0;
                price = rnd.Next() / 359772;
                var purchase = new OrderList(newProduct, price, quantity);

                //int* wannatry = &(purchase.Price);
                //*wannatry = 10;
                //Console.WriteLine(purchase.Price);


                newOrder.AddItem(purchase);
                Console.WriteLine("\n\nHere is your list of purchases: ");
                foreach (var item in newOrder.Items)
                {
                    Console.WriteLine("{0,-15} - {1,-15} - {2,-4} - {3,7}$\tTotal:{4,7}$",      // + align right
                                      item.Item.Type,                           // - align left
                                      item.Item.Model,
                                      item.Quantity,
                                      item.Price,
                                      item.Total);
                }

                Console.WriteLine("\nTtotal price: {0}$",newOrder.totalPrice);
                Console.WriteLine("\n\nDo you want to add something more?(if NO press N button)");

            } while (Console.ReadLine().ToUpper() != key);

        }
    }
}
