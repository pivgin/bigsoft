﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L4_HW_Classes_OrderList
{
    class Product
    {
        //private string type;
        //private string model;

        public Product(string type, string model)
        {
            this.Type = type;
            this.Model = model;
        }
        public string Type { get; set; }
        public string Model { get; set; }
    }
}
